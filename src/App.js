
//Native React

import React, { Component } from 'react';

//CSS
import './App.css';

//My components
import Image from './components/Image.js';
import ImageList from './components/ImageList.js';
// import AlbumList from './components/AlbumList.js';
import AlbumList from './components/PaperTest.js';
import AlbumDetails from './components/AlbumDetails.js';

//Material UI components
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

//Theme
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TestComponent from './components/testComponent.js';

class App extends Component {
constructor(props){
    super(props);
    this.state = {item: null}
  }


  newAlbumSelected = (item) =>{
      

      this.setState({albumImages: item.items, currAlbum: item});
  }

  render() {
       return <div>
              <MuiThemeProvider>
                   <AppBar title="Tripster" />
              </MuiThemeProvider>
               <AlbumList newAlbumSelected={this.newAlbumSelected}/>
               {this.state.albumImages ? <AlbumDetails album={this.state.currAlbum}/>:null}
               <br/> 
               {this.state.albumImages ? <ImageList images={this.state.albumImages}/> :null}
           </div>;
  }
}

export default App;
