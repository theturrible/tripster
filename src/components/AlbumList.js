import React, { Component } from 'react';

import Paper from 'material-ui/Paper';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class AlbumList extends Component{
  constructor(props){
        super(props);
        this.state = {albums: []};
        // this.state = {w: 200, h:200, id: this.props.id}
        // this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    //here we load
    fetch( "https://droog.vladof.ru/albums/" )
    .then( result=>result.json() )
    .then( albums=>this.setState({ albums: albums }) );
  }

  render(){
      // const listItems =  this.state.albums.map((alb) =>
      //   <li>{alb.title}</li>
      // );

      const numbers = [1, 2, 3, 4, 5];
      const listItems = numbers.map((number) =>
        <li>{number}</li>
      );

      console.log("ListItems:",  listItems);     
      return <div class="App-albumList">
      			 <ul> {listItems} </ul>
  			 </div>
  }
}


export default AlbumList;