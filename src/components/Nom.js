import React, { Component } from 'react';

// This is a simple field:value pair for display. NOTE: no empty itenName or null;
class Nom extends Component{
	render(){

		var itemName = this.props.itemName || "";
		var itemDesc = this.props.itemDesc || "";

		var style = {
			textAlign: "left",
			paddingLeft: "1em"


		}

		return <div style={style}> {itemName} : {itemDesc} </div>
	}


}

export default Nom;