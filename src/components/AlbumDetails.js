//Native React

import React, { Component } from 'react';

//CSS
import '../App.css';

//My components
import Image from '../components/Image.js';
// import AlbumList from './components/AlbumList.js';
import AlbumList from '../components/PaperTest.js'
import Nom from "../components/Nom.js";

//Material UI components
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

//Theme
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TestComponent from '../components/testComponent.js';
import PanelTitle from '../components/PanelTItle.js'



class AlbumDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { images: [] };
  
    }

  render() {

      var imgStyle ={
        padding: "1em"
      }

      const listItems =[];
      var al = this.props.album;
	Object.keys(al).forEach(function(key,index) {
		var itemDesc = al[key];
		listItems[index] = (<Nom itemName={key} itemDesc={itemDesc}/>);
	    // key: the name of the object key
	    // index: the ordinal position of the key within the object 
	});

     return <MuiThemeProvider> 
              <div className="App_AlbumList_display">
                 <PanelTitle zDepth={1} emptyTitle="Album Details" selAlbTitle={this.state.selAlbTitle}  />
                 <Paper style={{minHeight: "10em"}}zDepth={4}>
                 	{listItems}
                 </Paper>  
              </div>               
            </MuiThemeProvider>;
  }
}


export default AlbumDetails;