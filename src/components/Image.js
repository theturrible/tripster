import React, { Component } from 'react';

class Image extends Component{
    constructor(props){
      super(props);
      this.state = {w: 200, h:200, id: this.props.id}
      this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        if(this.state.details == null){
          fetch( "https://droog.vladof.ru/items/item/"+this.state.id )
          .then( result=>result.json() )
          .then( details=>this.setState({ details: details }) );
        }
    }

    render(){
      let w = this.props.w || this.state.w;
      let h = this.props.h || this.state.h;

      var sourceString = "https://droog.vladof.ru/images/render/"+ this.state.id +"?w=" + w + "&h=" + h;
      return <img
                style={this.props.style}
                onClick={this.handleClick}
                src={sourceString}
                alt={this.props.id}>
              </img>
    }
}

export default Image;