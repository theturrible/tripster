import React, { Component }  from 'react';
import Paper from 'material-ui/Paper';
//CSS
import '../App.css';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Divider from 'material-ui/Divider';
import Image from '../components/Image.js';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import PanelTitle from '../components/PanelTItle.js'

class AlbumList extends Component{
  constructor(props){
        super(props);

        this.state = {albums: [], selectedAlbum: "", selAlbTitle: ""};
        // this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    fetch( "https://droog.vladof.ru/albums/" )
    .then( result=>result.json() )
    .then( albums=>this.setState({ albums: albums }) );
  }

  /**
  ITEM IS ALBUM 
  **/
  handleAlbumClick = (item) =>{

    this.setState( (prevState, props) => {
        // notify app to update imagelist
        if(item._id != this.state.selectedAlbum){
          this.props.newAlbumSelected(item);
        }

        return {selectedAlbum: item._id, selAlbTitle: item.title};
    });
  } 

  render(){
      const listItems = this.state.albums.map((album) =>{
          var sel = this.state.selectedAlbum == album._id;
          return <AlbumBox album={album} key={album._id} selected={sel} thumb={album.items[0]} onSelChange={this.handleAlbumClick}/>

      });


        // const CardExampleWithAvatar = () => (
        //   <Card>
        //     <CardHeader
        //       title="URL Avatar"
        //       subtitle="Subtitle"
        //     />
        //     <CardMedia
        //       overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
        //     >
        //     </CardMedia>
        //     <CardTitle title="Card title" subtitle="Card subtitle" />
        //     <CardText>
        //       Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        //       Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
        //       Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
        //       Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
        //     </CardText>
        //     <CardActions>
        //       <FlatButton label="Action1" />
        //       <FlatButton label="Action2" />
        //     </CardActions>
        //   </Card>
        // );



      const style = {
          backgroundColor: this.state.selAlbTitle ? '#81D4FA' : '#E0E0E0'  
      }

      console.log(this.state.selAlbTitle);

      return <MuiThemeProvider> 
              <div className="App_AlbumList_display">
                  <PanelTitle style={style} zDepth={1} selAlbTitle={this.state.selAlbTitle} emptyTitle="Albums" /> 
                  <Paper zDepth={2} className="App_AlbumList_display"> 
                    <div>
                      {listItems}
                    </div>
                  </Paper>                
                </div>
             </MuiThemeProvider>;
  }
}


// TODO need better size management, otherwise its the images
class AlbumBox extends Component{
    constructor(props){
        super(props);
        let style = {
          height: 100,
          width: 100,
          margin: 20,
          textAlign: 'center',
          display: 'inline-block',
          overflow: 'hidden'
        };
        console.log(props.album);
        let albm = props.album ? props.album : {};


        this.state = {
                      details: albm, 
                      style: style,
                      zDepth: 3
                    };

    }

   handleClick = (e)=>{
      e.preventDefault();
      this.setState( (prevState, props) => {
          if(props.onSelChange) {
            props.onSelChange(props.album);
          }
      });
    }


    //props here will be a single album obj 
    /*  
      {
        "_id": "59324f1a73f8170001b1e35a",
        "title": "Test",
        "desc": "First album",
        "__v": 0,
        "tags": [],
        "items": [],
        "modified": "2017-06-03T05:54:34.743Z",
        "created": "2017-06-03T05:54:34.743Z"
      }
    */
    render(){
        let cli = this.props.selected  ? "App-image-sel" : "";

        return <Paper style={this.state.style} 
                      circle={true}
                      zDepth={this.state.zDepth} 
                      className={cli}
                      onClick={this.handleClick}>
                      <Image key={this.props.thumb} w={150} h={100} id={this.props.thumb}/>
                </Paper>;
    }
}



export default AlbumList;