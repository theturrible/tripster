import React, { Component }  from 'react';

class Header extends Component {
  constructor(props){
        super(props);
        this.state = {date: new Date()};
    }

  render() {
      return <div  id="head" class="App">Current Time: {this.state.date.toLocaleTimeString()}</div>;
    }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  componentDidMount() {
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
  }

  сomponentWillUnmount() {
    clearInterval(this.timerID);
  }
}

export default Header;