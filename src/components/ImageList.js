//Native React

import React, { Component } from 'react';

//CSS
import '../App.css';

//My components
import Image from '../components/Image.js';
// import AlbumList from './components/AlbumList.js';
import AlbumList from '../components/PaperTest.js'

//Material UI components
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

//Theme
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TestComponent from '../components/testComponent.js';
import PanelTitle from '../components/PanelTItle.js'


class ImageList extends Component {
  constructor(props) {
    super(props);
    this.state = { images: [] };
  
    }
  
  reloadData = () => {
    //here we load
    // console.log("fetchUrl", this.state.fetchURL);
    fetch( this.state.fetchURL )
    .then( result=>result.json() )
    .then( items=>this.setState({ images: items }) ); 
  }

  render() {

      var imgStyle ={
        padding: "1em"
      }

      const listItems = this.props.images.map((image) =>{
          return <Image key={image.name} id={image} style={imgStyle}/>;
      });

     return <MuiThemeProvider> 
              <div className="App_AlbumList_display">
                <PanelTitle zDepth={1} emptyTitle="Album Images" selAlbTitle={this.state.selAlbTitle}  /> 
                <Paper zDepth={4}> {listItems} </Paper> 
              </div>               
            </MuiThemeProvider>;
  }
}


export default ImageList;