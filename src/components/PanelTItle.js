import React, { Component }  from 'react';
import Paper from 'material-ui/Paper';


/**
Component will display a header. Set selAlbTitle for an alternative text
**/
class PanelTitle extends Component{
	
	render(){

		var style = this.props.style ? this.props.style : {
			backgroundColor: '#E0E0E0'  
		};

		return	 <Paper style={style} zDepth={1}> 
                  <p className="App_album_title"> 
                      {this.props.selAlbTitle ? "Selected Album: " + this.props.selAlbTitle : this.props.emptyTitle} 
                  </p>
  		</Paper>
  	}
}

export default PanelTitle;