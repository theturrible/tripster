import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const TestComponent = () => (
  <RaisedButton label="Default" />
);

export default TestComponent;